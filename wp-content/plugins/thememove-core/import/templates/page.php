<div class="wrap">
	<h2 class="tm-page-title"><?php echo esc_html( sprintf( __( '%s - One-Click Import', 'thememove' ), $theme ) ); ?></h2>
	<div class="update-nag">
		Please wait when the import is running! It will take time needed to download all attachments from demo web site.
	</div>
	<div class="tm-demo-source-container">
		<?php foreach ( $demos as $demo_id => $demo ) : ?>
			<div class="tm-demo-source">
				<div class="tm-demo-source-screenshot">
					<img src="<?php echo esc_url( $demo['screenshot'] ); ?>" alt="<?php echo esc_attr( $demo['name'] ); ?>">
				</div>
				<div class="tm-demo-source-heading">
					<h3 class="tm-demo-source-title"><?php echo esc_html( $demo['name'] ); ?></h3>
					<button class="tm-demo-source-install" data-demo="<?php echo esc_attr( $demo_id ); ?>">Install</button>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<div id="tm-import-result">
	</div>
</div>
<script type="text/html" id="tm-import-progress">
	<div id="tm-import-progress-<%= data %>" class="tm-import-progress">
		<h3><%= title %></h3>
		<div class="tm-import-progressbar"></div>
	</div>
</script>