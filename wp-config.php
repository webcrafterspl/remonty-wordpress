<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_remonty');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'Wordpress1234$$');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UCKWL,9@i;aE+]40pcf9Lik61pGA(Cg+7&&r<K4f;o>MB#9EJvH!10KQ-Zz.<%a5');
define('SECURE_AUTH_KEY',  '0fW}g1+[*0y72k>&*W(Fv1qs$urO=BwNkRP:l?s/e:D#W/Si<D*xSR4asf<JmIKD');
define('LOGGED_IN_KEY',    'wFIxQUM))vDKyt4b1;RovtRjiA#PB~ImJ]Bnhx7XFnPisIna$B=m=7X AiV:>8%#');
define('NONCE_KEY',        '*w+aGna:N>V:_=} >Y#!N{RAcq?m[;MDK>6Pg0}h`S~Z`MVZ]J#5f-S<pqc?oVDA');
define('AUTH_SALT',        'R5[K`NWuP_iGw$%ghv3u?>Pmqk/1~u8jh6H{:z1e!!=]] [<wc}%d61 /0l0>*^K');
define('SECURE_AUTH_SALT', 'tMY^,SVVJY8 j#`lph<l%n3|mYK({7H`GJGaWRJxn){ydwVzVgwD{.p+([e:As$C');
define('LOGGED_IN_SALT',   ']@G=OL:A!5N0ZL_G+g(|&4),QXtMwd(D1Z32Qkk;D@mNY=CgCLjlL.F &R7$l0$!');
define('NONCE_SALT',       'TfO,8%gu3O.hOW`XH`*oX{l+PM+wqA?rmmd+@P/rz{b0~p$K8Bey! ,M0{EI<$|X');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
